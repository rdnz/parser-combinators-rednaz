# error accumulation

## benchmark

- install dependencies
  - GHC 9.4.3
  - `cabal-install`
  - python 3.10 with `numpy`, `pandas`, and `matplotlib`
- execute `cd benchmark_results`
- execute `./all_benchmarks.sh` to benchmark and generate result files
- execute `python3 main.py` to generate plots of the result files
