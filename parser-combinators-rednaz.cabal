cabal-version:   2.4
name:            parser-combinators-rednaz
version:         0.1.0.0
license:         Unlicense
license-file:    LICENSE
author:          rednaZ
maintainer:      p.rednaz@googlemail.com
category:        Parsing
build-type:      Simple
extra-doc-files: CHANGELOG.md

common shared-properties
  default-language:   GHC2021
  default-extensions:
    NoFieldSelectors
    BlockArguments
    DerivingStrategies
    LambdaCase
    OverloadedStrings

  ghc-options:
    -Weverything -Wno-missing-safe-haskell-mode -Wno-unsafe
    -Wno-missing-export-lists -Wno-missing-kind-signatures
    -Wno-implicit-prelude -Wno-all-missed-specialisations

library
  import:          shared-properties

  -- cabal-fmt: expand src
  exposed-modules:
    Control.Monad.Writer.CPS
    Data.DifferenceList
    Text.Parse.Byte
    Text.Parse.Byte.Help
    Text.Parse.Char
    Text.Parse.Char.AttoparsecInterface
    Text.Parse.Char.Help
    Utilities

  build-depends:
    , base                ==4.17.0.0
    , bytestring          ==0.11.3.1
    , case-insensitive    ==1.2.1.0
    , deepseq             ==1.4.8.0
    , mtl                 ==2.2.2
    , parser-combinators  ==1.3.0
    , scientific          ==0.3.7.0
    , text                ==2.0.2
    , transformers        ==0.5.6.2

  hs-source-dirs:  src
  ghc-options:     -Werror

test-suite parser-combinators-rednaz-test
  import:         shared-properties
  other-modules:  ArithmeticParser
  type:           exitcode-stdio-1.0
  hs-source-dirs: test
  main-is:        Main.hs
  build-depends:
    , base                       ==4.17.0.0
    , parser-combinators-rednaz
    , text                       ==2.0.2

test-suite dynasty
  import:         shared-properties
  type:           exitcode-stdio-1.0
  other-modules:
    Dynasty.Megaparsec
    Dynasty.Rednaz
    Dynasty.Syntax

  hs-source-dirs: dynasty
  main-is:        Main.hs
  build-depends:
    , base                       ==4.17.0.0
    , directory                  ==1.3.7.1
    , megaparsec                 ==9.3.0
    , parser-combinators         ==1.3.0
    , parser-combinators-rednaz
    , scientific                 ==0.3.7.0
    , text                       ==2.0.2

  ghc-options:
    -Wno-missing-import-lists -Wno-missing-import-lists
    -Wno-missed-specialisations

benchmark bench0-speed
  type:             exitcode-stdio-1.0
  main-is:          Main.hs
  hs-source-dirs:   bench0/speed
  default-language: Haskell2010
  build-depends:
    , base                       ==4.17.0.0
    , criterion                  ==1.6.0.0
    , deepseq                    ==1.4.8.0
    , megaparsec                 ==9.3.0
    , parser-combinators         ==1.3.0
    , parser-combinators-rednaz
    , text                       ==2.0.2

  ghc-options:
    -Weverything -Wno-missing-safe-haskell-mode -Wno-unsafe
    -Wno-missing-export-lists -Wno-missing-kind-signatures
    -Wno-implicit-prelude -Wno-all-missed-specialisations
    -Wno-missed-specialisations -Wno-missing-import-lists
    -Wno-prepositive-qualified-module

benchmark bench0-memory
  type:             exitcode-stdio-1.0
  main-is:          Main.hs
  hs-source-dirs:   bench0/memory
  default-language: Haskell2010
  build-depends:
    , base                       ==4.17.0.0
    , deepseq                    ==1.4.8.0
    , megaparsec                 ==9.3.0
    , parser-combinators         ==1.3.0
    , parser-combinators-rednaz
    , text                       ==2.0.2
    , weigh                      ==0.0.16

  ghc-options:
    -Weverything -Wno-missing-safe-haskell-mode -Wno-unsafe
    -Wno-missing-export-lists -Wno-missing-kind-signatures
    -Wno-implicit-prelude -Wno-all-missed-specialisations
    -Wno-missed-specialisations -Wno-missing-import-lists
    -Wno-prepositive-qualified-module

benchmark bench1-speed
  type:             exitcode-stdio-1.0
  main-is:          Speed.hs
  hs-source-dirs:   bench1
  default-language: Haskell2010
  build-depends:
    , attoparsec                 ==0.14.4
    , base                       ==4.17.0.0
    , bytestring                 ==0.11.3.1
    , criterion                  ==1.6.0.0
    , deepseq                    ==1.4.8.0
    , megaparsec                 ==9.3.0
    , parser-combinators         ==1.3.0
    , parser-combinators-rednaz
    , scientific                 ==0.3.7.0
    , text                       ==2.0.2
    , time                       ==1.12.2
    , unordered-containers       ==0.2.19.1
    , vector                     ==0.13.0.0

  ghc-options:
    -Weverything -Wno-missing-safe-haskell-mode -Wno-unsafe -Wno-safe
    -Wno-missing-export-lists -Wno-missing-kind-signatures
    -Wno-implicit-prelude -Wno-all-missed-specialisations
    -Wno-missed-specialisations -Wno-missing-import-lists
    -Wno-prepositive-qualified-module -Wno-missing-deriving-strategies

  other-modules:
    ParsersBench.CSV.Attoparsec
    ParsersBench.CSV.Megaparsec
    ParsersBench.CSV.Rednaz
    ParsersBench.Json.Attoparsec
    ParsersBench.Json.Common
    ParsersBench.Json.Megaparsec
    ParsersBench.Json.Rednaz
    ParsersBench.Log.Attoparsec
    ParsersBench.Log.Common
    ParsersBench.Log.Megaparsec
    ParsersBench.Log.Rednaz

benchmark bench1-memory
  type:             exitcode-stdio-1.0
  main-is:          Memory.hs
  hs-source-dirs:   bench1
  default-language: Haskell2010
  build-depends:
    , attoparsec                 ==0.14.4
    , base                       ==4.17.0.0
    , bytestring                 ==0.11.3.1
    , deepseq                    ==1.4.8.0
    , megaparsec                 ==9.3.0
    , parser-combinators         ==1.3.0
    , parser-combinators-rednaz
    , scientific                 ==0.3.7.0
    , text                       ==2.0.2
    , time                       ==1.12.2
    , unordered-containers       ==0.2.19.1
    , vector                     ==0.13.0.0
    , weigh                      ==0.0.16

  ghc-options:
    -Weverything -Wno-missing-safe-haskell-mode -Wno-unsafe -Wno-safe
    -Wno-missing-export-lists -Wno-missing-kind-signatures
    -Wno-implicit-prelude -Wno-all-missed-specialisations
    -Wno-missed-specialisations -Wno-missing-import-lists
    -Wno-prepositive-qualified-module -Wno-missing-deriving-strategies

  other-modules:
    ParsersBench.CSV.Attoparsec
    ParsersBench.CSV.Megaparsec
    ParsersBench.CSV.Rednaz
    ParsersBench.Json.Attoparsec
    ParsersBench.Json.Common
    ParsersBench.Json.Megaparsec
    ParsersBench.Json.Rednaz
    ParsersBench.Log.Attoparsec
    ParsersBench.Log.Common
    ParsersBench.Log.Megaparsec
    ParsersBench.Log.Rednaz
