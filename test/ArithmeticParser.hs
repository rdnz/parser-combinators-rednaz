module ArithmeticParser where

import Text.Parse.Char (Parser, GroupedParseError, runParser, eof, single, takeWhileP)
import Text.Parse.Char.Help qualified as L

import Control.Applicative (Alternative (many, (<|>)))
import Control.Monad (unless)
import Data.Char (isSpace)
import Data.Foldable (Foldable (foldl'))
import Data.Text (Text)
import Numeric.Natural (Natural)
import System.Exit (exitFailure)
import Data.List.NonEmpty (NonEmpty)

lexeme :: Parser a -> Parser a
lexeme = L.lexeme L.space

{-
Backus–Naur form

expression  ::= expression1 (\+ expression1)*
expression1 ::= expression2 (\* expression2)*
expression2 ::= \( expression \) | number

number ::= digit+

-}

data Expression =
  Literal Natural |
  Sum Expression Expression |
  Product Expression Expression
  deriving stock (Eq, Show)

expression :: Parser Expression
expression =
  foldl' Sum
    <$> expression1
    <*> many (lexeme (single '+') *> expression1)

expression1 :: Parser Expression
expression1 =
  foldl' Product
    <$> expression2
    <*> many (lexeme (single '*') *> expression2)

expression2 :: Parser Expression
expression2 =
  Literal <$> lexeme L.decimal
  <|>
  lexeme (single '(') *> expression <* lexeme (single ')')

main :: IO ()
main = do
  parseExpression "3*12" `shouldBe` Right (Product (Literal 3) (Literal 12))
  parseExpression " 3 + 12 " `shouldBe` Right (Sum (Literal 3) (Literal 12))
  parseExpression "3 * (12 + 2)" `shouldBe`
    Right (Product (Literal 3) (Sum (Literal 12) (Literal 2)))
  parseExpression "(3 * 12) + 2" `shouldBe`
    Right (Sum (Product (Literal 3) (Literal 12)) (Literal 2))
  parseExpression "3 * 12 + 2" `shouldBe`
    Right (Sum (Product (Literal 3) (Literal 12)) (Literal 2))
  parseExpression "12 + 3 * 6" `shouldBe`
    Right (Sum (Literal 12) (Product (Literal 3) (Literal 6)))
  parseExpression "1 + 2 + 3 + 4 + 5" `shouldBe`
    Right (Sum (Sum (Sum (Sum (Literal 1) (Literal 2)) (Literal 3)) (Literal 4)) (Literal 5))
  parseExpression " 1 +  2+ 3 *4+5 " `shouldBe`
    Right (Sum (Sum (Sum (Literal 1) (Literal 2)) (Product (Literal 3) (Literal 4))) (Literal 5))
  parseExpression "(1 + (  ((2) * 7)) + 3 )*(4+5)" `shouldBe`
    Right (Product (Sum (Sum (Literal 1) (Product (Literal 2) (Literal 7))) (Literal 3)) (Sum (Literal 4) (Literal 5)))

parseExpression ::
  Text -> Either [(Text, NonEmpty GroupedParseError)] Expression
parseExpression = runParser (takeWhileP isSpace *> expression <* eof)

shouldBe :: (Eq a) => a -> a -> IO ()
shouldBe a b = unless (a == b) exitFailure
