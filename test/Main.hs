module Main where

import Text.Parse.Char (Parser, ParseError (ParseError), runParser, runParserVerbose, char, (<?>), errorBundlePretty)
import Text.Parse.Char.Help (try)
import ArithmeticParser (shouldBe)
import ArithmeticParser qualified
import Utilities ((.:))

import Control.Applicative (Alternative ((<|>)))
import Data.Text (Text)
import Data.Foldable (Foldable (toList))

main :: IO ()
main = do
  ArithmeticParser.main

  -- backtracking
  parseTest
    ((pure ' ' <|> char 'a') *> char 'b' :: Parser Char)
    "ab"
  --   |
  -- 1 | ab
  --   | ^
  -- unexpected 'a'
  -- expecting 'b'
  parseTest
    (char 'a' *> char 'b' <|> pure ' ' :: Parser Char)
    "a"
  -- ' '
  parseTest
    (try (char 'a' *> char 'b') <|> pure ' ' :: Parser Char)
    "a"
  -- ' '

  -- error information
  fmap toList
    (runParserVerbose
      ((traverse char "abcde" <|> pure "") <* char '.' :: Parser String)
      "abce."
    )
    `shouldBe` (Nothing, [ParseError "d" "e.", ParseError "." "abce."])

  parseTest
    ((traverse char "abcde" <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   |    ^
  -- unexpected 'e'
  -- expecting 'd'
  -- or
  --   |
  -- 1 | abce.
  --   | ^
  -- unexpected 'a'
  -- expecting '.'
  parseTest
    ((try (traverse char "abcde") <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   |    ^
  -- unexpected 'e'
  -- expecting 'd'
  -- or
  --   |
  -- 1 | abce.
  --   | ^
  -- unexpected 'a'
  -- expecting '.'
  parseTest
    (((try (traverse char "abcde") <?> "abcde") <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   |    ^
  -- unexpected 'e'
  -- expecting 'd'
  -- or
  --   |
  -- 1 | abce.
  --   | ^
  -- unexpected 'a'
  -- expecting '.'
  parseTest
    (try (char 'a' *> char 'b') <|> char 'a' *> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   |  ^
  -- unexpected 'd'
  -- expecting 'b' or 'c'
  parseTest
    (try (char 'a' *> char 'b') <|> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   |  ^
  -- unexpected 'd'
  -- expecting 'b'
  -- or
  --   |
  -- 1 | ad
  --   | ^
  -- unexpected 'a'
  -- expecting 'c'
  parseTest
    ((try (char 'a' *> char 'b') <?> "ab") <|> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   |  ^
  -- unexpected 'd'
  -- expecting ab
  -- or
  --   |
  -- 1 | ad
  --   | ^
  -- unexpected 'a'
  -- expecting 'c'

parseTest :: (Show a) => Parser a -> Text -> IO ()
parseTest =
  either (putStrLn . errorBundlePretty) (\r -> print r *> putStrLn "")
  .:
  runParser
