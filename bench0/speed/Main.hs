{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Main (main) where

import Control.DeepSeq
import Criterion.Main
import Data.Text (Text)
import qualified Data.Text as T
import Data.Void
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char as MC
import qualified Text.Megaparsec.Char.Lexer as ML
import qualified Text.Parse.Char as R
import qualified Text.Parse.Char.Help as RH
import Control.Monad.Combinators

-- | The type of parser that consumes 'Text'.
type ParserM = M.Parsec Void Text
type ParserR = R.Parser

main :: IO ()
main =
  defaultMain
    [
      bparserM "Megaparsec/string" manyAs (MC.string . fst),
      bparserR "Rednaz/string" manyAs (R.string . fst),

      bparserM "Megaparsec/string'" manyAs (MC.string' . fst),
      bparserR "Rednaz/string'" manyAs (RH.string' . fst),

      bparserM "Megaparsec/takeWhileP" manyAs (const $ M.takeWhileP Nothing (== 'a')),
      bparserR "Rednaz/takeWhileP" manyAs (const $ R.takeWhileP (== 'a')),

      bparserM "Megaparsec/takeWhile1P" manyAs (const $ M.takeWhile1P Nothing (== 'a')),
      bparserR "Rednaz/takeWhile1P" manyAs (const $ R.takeWhile1P Nothing (== 'a')),

      bparserM "Megaparsec/count" manyAs (\(_, n) -> count n (MC.char 'a')),
      bparserR "Rednaz/count" manyAs (\(_, n) -> count n (R.char 'a')),

      bparserM "Megaparsec/count'" manyAs (\(_, n) -> count' 1 n (MC.char 'a')),
      bparserR "Rednaz/count'" manyAs (\(_, n) -> count' 1 n (R.char 'a')),

      bparserM "Megaparsec/endBy" manyAbs' (const $ endBy (MC.char 'a') (MC.char 'b')),
      bparserR "Rednaz/endBy" manyAbs' (const $ endBy (R.char 'a') (R.char 'b')),

      bparserM "Megaparsec/endBy1" manyAbs' (const $ endBy1 (MC.char 'a') (MC.char 'b')),
      bparserR "Rednaz/endBy1" manyAbs' (const $ endBy1 (R.char 'a') (R.char 'b')),

      bparserM "Megaparsec/sepEndBy" manyAbs' (const $ sepEndBy (MC.char 'a') (MC.char 'b')),
      bparserR "Rednaz/sepEndBy" manyAbs' (const $ sepEndBy (R.char 'a') (R.char 'b')),

      bparserM "Megaparsec/sepEndBy1" manyAbs' (const $ sepEndBy1 (MC.char 'a') (MC.char 'b')),
      bparserR "Rednaz/sepEndBy1" manyAbs' (const $ sepEndBy1 (R.char 'a') (R.char 'b')),

      bparserM "Megaparsec/skipMany" manyAs (const $ skipMany (MC.char 'a')),
      bparserR "Rednaz/skipMany" manyAs (const $ skipMany (R.char 'a')),

      bparserM "Megaparsec/skipSome" manyAs (const $ skipSome (MC.char 'a')),
      bparserR "Rednaz/skipSome" manyAs (const $ skipSome (R.char 'a')),

      bparserM "Megaparsec/many" manyAs (const $ many (MC.char 'a')),
      bparserR "Rednaz/many" manyAs (const $ many (R.char 'a')),

      bparserM "Megaparsec/some" manyAs (const $ some (MC.char 'a')),
      bparserR "Rednaz/some" manyAs (const $ some (R.char 'a')),

      bparserM "Megaparsec/choice" (const "b") (choice . fmap MC.char . manyAsB' . snd),
      bparserR "Rednaz/choice" (const "b") (choice . fmap R.char . manyAsB' . snd),

      bparserM "Megaparsec/manyTill" manyAsB (const $ manyTill (MC.char 'a') (MC.char 'b')),
      bparserR "Rednaz/manyTill" manyAsB (const $ manyTill (R.char 'a') (R.char 'b')),

      bparserM "Megaparsec/someTill" manyAsB (const $ someTill (MC.char 'a') (MC.char 'b')),
      bparserR "Rednaz/someTill" manyAsB (const $ someTill (R.char 'a') (R.char 'b')),

      bparserM "Megaparsec/sepBy" manyAbs (const $ sepBy (MC.char 'a') (MC.char 'b')),
      bparserR "Rednaz/sepBy" manyAbs (const $ sepBy (R.char 'a') (R.char 'b')),

      bparserM "Megaparsec/sepBy1" manyAbs (const $ sepBy1 (MC.char 'a') (MC.char 'b')),
      bparserR "Rednaz/sepBy1" manyAbs (const $ sepBy1 (R.char 'a') (R.char 'b')),

      bparserM "Megaparsec/skipCount" manyAs (\(_, n) -> skipCount n (MC.char 'a')),
      bparserR "Rednaz/skipCount" manyAs (\(_, n) -> skipCount n (R.char 'a')),

      bparserM "Megaparsec/skipManyTill" manyAsB (const $ skipManyTill (MC.char 'a') (MC.char 'b')),
      bparserR "Rednaz/skipManyTill" manyAsB (const $ skipManyTill (R.char 'a') (R.char 'b')),

      bparserM "Megaparsec/skipSomeTill" manyAsB (const $ skipSomeTill (MC.char 'a') (MC.char 'b')),
      bparserR "Rednaz/skipSomeTill" manyAsB (const $ skipSomeTill (R.char 'a') (R.char 'b')),

      bparserM "Megaparsec/decimal" mkInt (const (ML.decimal :: ParserM Integer)),
      bparserR "Rednaz/decimal" mkInt (const (RH.decimal :: ParserR Integer)),

      -- bparserM "octal" mkInt (const (L.octal :: ParserM Integer)),
      -- bparserM "hexadecimal" mkInt (const (L.hexadecimal :: ParserM Integer)),

      bparserM "Megaparsec/scientific" mkInt (const ML.scientific),
      bparserR "Rednaz/scientific" mkInt (const RH.scientific)

      -- bparserMBs "word32be" many0x33 (const $ many Binary.word32be),
      -- bparserMBs "word32le" many0x33 (const $ many Binary.word32le),
    ]

-- | Perform a series to measurements with the same parser.
bparserM ::
  (NFData a) =>
  -- | Name of the benchmark group
  String ->
  -- | How to construct input
  (Int -> Text) ->
  -- | The parser receiving its future input
  ((Text, Int) -> ParserM a) ->
  -- | The benchmark
  Benchmark
bparserM name f p = bgroup name (bs <$> stdSeries)
  where
    bs n = env (return (f n, n)) (bench (show n) . nf p')
    p' (s, n) = M.parse (p (s, n)) "" s

-- | Perform a series to measurements with the same parser.
bparserR ::
  (NFData a) =>
  -- | Name of the benchmark group
  String ->
  -- | How to construct input
  (Int -> Text) ->
  -- | The parser receiving its future input
  ((Text, Int) -> ParserR a) ->
  -- | The benchmark
  Benchmark
bparserR name f p = bgroup name (bs <$> stdSeries)
  where
    bs n = env (return (f n, n)) (bench (show n) . nf p')
    p' (s, n) = R.runParser (p (s, n)) s

-- | The series of sizes to try as part of 'bparser'.
stdSeries :: [Int]
stdSeries = [500, 1000, 2000, 4000]

----------------------------------------------------------------------------
-- Helpers

-- | Generate that many \'a\' characters.
manyAs :: Int -> Text
manyAs n = T.replicate n "a"

-- | Like 'manyAs', but interspersed with \'b\'s.
manyAbs :: Int -> Text
manyAbs n = T.take (if even n then n + 1 else n) (T.replicate n "ab")

-- | Like 'manyAs', but with a \'b\' added to the end.
manyAsB :: Int -> Text
manyAsB n = manyAs n <> "b"

-- | Like 'manyAsB', but returns a 'String'.
manyAsB' :: Int -> String
manyAsB' n = replicate n 'a' ++ "b"

-- | Like 'manyAbs', but ends in a \'b\'.
manyAbs' :: Int -> Text
manyAbs' n = T.take (if even n then n else n + 1) (T.replicate n "ab")

-- | Render an 'Integer' with the number of digits linearly dependent on the
-- argument.
mkInt :: Int -> Text
mkInt n = (T.pack . show) ((10 :: Integer) ^ (n `quot` 100))
