cabal run bench0-speed # warmup
cabal run bench0-speed -- --json bench0-speed.json | tee bench0-speed.log
cabal run bench1-speed -- --json bench1-speed.json | tee bench1-speed.log
cabal run bench0-memory -- --markdown | tee bench0-memory.md
cabal run bench1-memory -- --markdown | tee bench1-memory.md
