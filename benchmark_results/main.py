import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import json as j
import re
from itertools import groupby
from operator import itemgetter

# def groupby(iterable, key): # to do. remove
#     return tuple((a, tuple(b)) for (a, b) in itertools.groupby(iterable, key))

bench_name1 = re.compile(r"[^(]*\(([^)]+)\)[/-](.+)-(\d+)\..*")
bench_name_speed0 = re.compile(r"([^/]+)/([^/]+)/([^/]+)")
bench_name_memory0 = re.compile(r"([^-]+)-([^-]+)-([^/]+)")

def group_sorted(iterable, key):
    return groupby(sorted(iterable, key=key), key=key)

def transpose_dictionaries(dictionary):
    """
    transpose a dictionary of dictionaries. for example, turn
    `{"a": {"c": 0, "d": 1}, "b": {"c": 2, "d": 3}}` into
    `{"c": {"a": 0, "b": 2}, "d": {"a": 1, "b": 3}}`.
    """
    return {
        k1: {
            r1[0]: r1[2]
            for r1 in r0
        }
        for (k1, r0) in group_sorted(
            (
                (k0, k1, v1)
                for (k0, v0) in dictionary.items()
                for (k1, v1) in v0.items()
            ),
            key=itemgetter(1)
        )
    }

def parse_bench_name(name):
    reMatch = bench_name1.fullmatch(name)
    if reMatch is None:
        reMatch = bench_name_speed0.fullmatch(name)
        if reMatch is None:
            reMatch = bench_name_memory0.fullmatch(name)
    return {
        "library": "error accumulation" if reMatch[1] == "Rednaz" else reMatch[1].lower(),
        "grammar": reMatch[2],
        "input": int(reMatch[3])
    }

def unmarshal_speed(benchmarks):
    """
    unmarshals to the following shape.

    {"string": {500: {"error accumulation": 0.38561290569478285,
                      "megaparsec": 0.4973198441612616},
                1000: {"error accumulation": 0.6617164389311493,
                       "megaparsec": 0.7733381483842295},
                2000: {"error accumulation": 1.2625243996074444,
                       "megaparsec": 1.367987776735256},
                4000: {"error accumulation": 2.4297940610620854,
                       "megaparsec": 2.532344783954995}},
     "string'": {500: {"error accumulation": 8.578124720512585,
                       "megaparsec": 8.642797254199108},
                 1000: {"error accumulation": 16.954865693877693,
                        "megaparsec": 17.011775896092246},
                 ...},
     ...}
    """
    return {
        g: {
            i: {
                data2["library"]: data2["value"] for data2 in data1
            }
            for (i, data1) in group_sorted(data0, key=itemgetter("input"))
        }
        for (g, data0) in groupby(
            (
                {**parse_bench_name(c["reportName"]), "value": c["reportAnalysis"]["anRegress"][0]["regCoeffs"]["iters"]["estPoint"] * 1000000}
                for c in benchmarks
            ),
            key=itemgetter("grammar")
        )
    }

def unmarshal_memory(benchmarks):
    """
    unmarshals to the following shape.

    {"csv": {5: {"attoparsec": {"allocated": 89744, "max": 11136},
                 "error accumulation": {"allocated": 100576, "max": 11184},
                 "megaparsec": {"allocated": 100576, "max": 11184}},
             10: {"attoparsec": {"allocated": 157816, "max": 12856},
                  "error accumulation": {"allocated": 179016, "max": 12904},
                  "megaparsec": {"allocated": 179016, "max": 12904}},
             20: {"attoparsec": {"allocated": 298048, "max": 20384},
                  "error accumulation": {"allocated": 339968, "max": 20416},
                  "megaparsec": {"allocated": 339984, "max": 20432}},
             40: {"attoparsec": {"allocated": 572744, "max": 27240},
                  "error accumulation": {"allocated": 656136, "max": 27288},
                  "megaparsec": {"allocated": 656152, "max": 27288}}},
     "json": {5: {"attoparsec": {"allocated": 30976, "max": 10080},
                  "error accumulation": {"allocated": 46696, "max": 10080},
                  "megaparsec": {"allocated": 46696, "max": 10080}},
              ...},
     ...}
    """
    return {
        g: {
            i: {
                data2["library"]: data2["value"] for data2 in data1
            }
            for (i, data1) in group_sorted(data0, key=itemgetter("input"))
        }
        for (g, data0) in groupby(
            (
                {**parse_bench_name(c["Case"]), "value": {"allocated": int(c["Allocated"].replace(",", "")), "max": int(c["Max"].replace(",", ""))}}
                for c in benchmarks
            ),
            key=itemgetter("grammar")
        )
    }

def plot(x_label, y_label, size, benchmarks, x_label_usetex=False, xtick_usetex=False, rotation=None):
    """
    plots data of the following shape.
    
    {500: {"error accumulation": 0.38561290569478285,
           "megaparsec": 0.4973198441612616},
     1000: {"error accumulation": 0.6617164389311493,
            "megaparsec": 0.7733381483842295},
     2000: {"error accumulation": 1.2625243996074444,
            "megaparsec": 1.367987776735256},
     4000: {"error accumulation": 2.4297940610620854,
            "megaparsec": 2.532344783954995}}
    """
    tick_labels = benchmarks.keys()
    benchmarks = transpose_dictionaries(benchmarks)
    (fig, ax) = plt.subplots(layout="constrained", figsize=size, dpi=150)
    library_count = len(benchmarks)
    width = 1 / (library_count+1)
    for (library_index, (library, data)) in enumerate(sorted(benchmarks.items(), key=lambda a: "z" if a[0] == "attoparsec" else a[0])): # weird sort to keep library_indexes consistent over cases with and without `attoparsec`
        x = np.arange(len(data))
        ax.bar(x + library_index * width, data.values(), width, label=library)
    ax.set_ylabel(y_label)
    ax.set_xlabel(x_label, usetex=x_label_usetex)
    ax.set_xticks(x + (library_count-1) * width / 2 , tick_labels, usetex=xtick_usetex, rotation=rotation)
    ax.legend(loc="upper left", ncol=library_count)
    return fig

with open("bench0-speed.json") as f0:
    with open("bench1-speed.json") as f1:
        speed = {**unmarshal_speed(j.load(f0)[2]), **unmarshal_speed(j.load(f1)[2])}

memory = {
    **unmarshal_memory(pd.read_table("bench0-memory.md", sep="|", usecols=(1, 2, 4), skiprows=(1,)).to_dict("records")),
    **unmarshal_memory(pd.read_table("bench1-memory.md", sep="|", usecols=(1, 2, 4), skiprows=(1,)).to_dict("records")),
}

def plot_speed_and_save(name, x_label, size, x_label_usetex=False):
    return plot(
        x_label,
        "time in microseconds",
        size,
        speed[name],
        x_label_usetex
    ).savefig(f"bench_{name}.png")

# plot_speed_and_save("many", "input size in characters", (6.4, 4.8))
# plot_speed_and_save("choice", r"parser size in length of \texttt{choice}'s argument", (6.4, 4.8), True)
# plot_speed_and_save("manyTill", "input size in characters", (6.4, 4.8))
# plot_speed_and_save("sepBy", "input size in characters", (6.4, 4.8))
# plot_speed_and_save("scientific", "input size in digit count times 100", (6.4, 4.8))
# plot_speed_and_save("skipMany", "input size in characters", (6.4, 4.8))
# plot_speed_and_save("csv", "input size in lines", (6.4, 4.8))
# plot_speed_and_save("log", "input size in lines", (6.4, 4.8))
# plot_speed_and_save("json", "input size in lines", (6.4, 4.8))

plot(
    "parser, input size in lines",
    "time in microseconds",
    (6.4, 4.8),
    {
        f"{parser}, {input}": data1
        for (parser, data0) in speed.items()
        for (input, data1) in data0.items()
        if parser in ("csv", "log", "json")
    },
    rotation=45
).savefig("bench_attoparsec.png")

plot(
    "parser, input size in characters",
    "time in microseconds",
    (6.4, 4.8),
    {
        f"\\texttt{{{parser}}}, {input}": data1
        for (parser, data0) in speed.items()
        for (input, data1) in data0.items()
        if parser in ("string", "decimal", "scientific")
    },
    xtick_usetex=True,
    rotation=45,
).savefig("bench_fast.png")

plot(
    "parser, input or parser size",
    "time in microseconds",
    (12, 6),
    {
        f"\\texttt{{{parser}}}, {input}": data1
        for (parser, data0) in speed.items()
        for (input, data1) in data0.items()
        if parser not in ("csv", "log", "json", "string", "decimal", "scientific")
    },
    xtick_usetex=True,
    rotation=90,
).savefig("bench_rest.png")

plot(
    "parser, input size in characters",
    "time in microseconds",
    (6.4, 4.8),
    {
        f"\\texttt{{{parser}}}, {input}": data1
        for (parser, data0) in speed.items()
        for (input, data1) in data0.items()
        if parser in ("many", "manyTill", "skipMany")
    },
    xtick_usetex=True,
    rotation=45
).savefig("bench_manys.png")

# plot(
#     "parser, input or parser size",
#     "time in microseconds",
#     (12, 6),
#     {
#         f"\\texttt{{{parser}}}, {input}": {"megaparsec - error accumulation": (data1["megaparsec"] - data1["error accumulation"])/input}
#         for (parser, data0) in speed.items()
#         for (input, data1) in data0.items()
#         if parser not in ("csv", "log", "json", "string", "decimal", "scientific")
#     },
#     xtick_usetex=True,
#     rotation=90,
# ).savefig("bench_relative_difference.png")

# plot(
#     "parser, input size in lines",
#     "time in microseconds",
#     (6.4, 4.8),
#     {
#         f"{parser}, {input}": {"megaparsec - error accumulation": (data1["megaparsec"] - data1["error accumulation"])/input}
#         for (parser, data0) in speed.items()
#         for (input, data1) in data0.items()
#         if parser in ("csv", "log", "json")
#     },
# ).savefig("bench_relative_difference_attoparsec.png")

plot(
    "parser, input size in characters",
    "total allocations in bytes",
    (6.4, 4.8),
    {
        f"\\texttt{{{parser}}}, {input}": transpose_dictionaries(data1)["allocated"]
        for (parser, data0) in memory.items()
        for (input, data1) in data0.items()
        if parser in ("csv", "log", "json")
    },
    xtick_usetex=True,
    rotation=45
).savefig(f"bench_memory_allocated_attoparsec.png")

plot(
    "parser, input size in characters",
    "total allocations in bytes",
    (6.4, 4.8),
    {
        f"\\texttt{{{parser}}}, {input}": transpose_dictionaries(data1)["allocated"]
        for (parser, data0) in memory.items()
        for (input, data1) in data0.items()
        if parser in ("string", "string'", "decimal", "scientific")
    },
    xtick_usetex=True,
    rotation=45
).savefig(f"bench_memory_allocated_lightweight.png")

plot(
    "parser, input size in characters",
    "total allocations in bytes",
    (12, 6),
    {
        f"\\texttt{{{parser}}}, {input}": transpose_dictionaries(data1)["allocated"]
        for (parser, data0) in memory.items()
        for (input, data1) in data0.items()
        if parser not in ("csv", "log", "json", "string", "string'", "decimal", "scientific")
    },
    xtick_usetex=True,
    rotation=90
).savefig(f"bench_memory_allocated_rest.png")

plot(
    "parser, input size in lines",
    "maximum residency memory in use in bytes",
    (6.4, 4.8),
    {
        f"\\texttt{{{parser}}}, {input}": transpose_dictionaries(data1)["max"]
        for (parser, data0) in memory.items()
        for (input, data1) in data0.items()
        if parser in ("csv", "log", "json")
    },
    xtick_usetex=True,
    rotation=45
).savefig(f"bench_memory_max_attoparsec.png")

plot(
    "parser, input size in characters",
    "maximum residency memory in use in bytes",
    (6.4, 4.8),
    {
        f"\\texttt{{{parser}}}, {input}": transpose_dictionaries(data1)["max"]
        for (parser, data0) in memory.items()
        for (input, data1) in data0.items()
        if parser in ("string", "string'", "decimal", "scientific")
    },
    xtick_usetex=True,
    rotation=45
).savefig(f"bench_memory_max_lightweight.png")

plot(
    "parser, input size in characters",
    "maximum residency memory in use in bytes",
    (12, 6),
    {
        f"\\texttt{{{parser}}}, {input}": transpose_dictionaries(data1)["max"]
        for (parser, data0) in memory.items()
        for (input, data1) in data0.items()
        if parser not in ("csv", "log", "json", "string", "string'", "decimal", "scientific")
    },
    xtick_usetex=True,
    rotation=90
).savefig(f"bench_memory_max_rest.png")

# plt.show()
