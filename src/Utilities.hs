{-# options_ghc -Wno-name-shadowing #-}

module Utilities where

import Control.Monad.State.Strict (StateT (runStateT), get, put)
import Control.Monad.Trans.Maybe (MaybeT (runMaybeT))

catch ::
  (Monad m) =>
  StateT state (MaybeT m) a ->
  StateT state (MaybeT m) a ->
  StateT state (MaybeT m) a
m `catch` handler =
  do
    s <- get
    join $
      lift $
      lift $
      fmap (\case
        Nothing -> handler
        Just (r, s) -> put s *> pure r
      ) $
      runMaybeT $
      runStateT m s
