{-# options_ghc
  -Wno-prepositive-qualified-module
  -Wno-missing-import-lists
  -Wno-orphans
#-}

-- copied from `mtl` version 2.3.1 because `weigh` forces us to use an `mtl` version <2.3

module Control.Monad.Writer.CPS (
    -- * MonadWriter class
    MonadWriter.MonadWriter(..),
    MonadWriter.listens,
    MonadWriter.censor,
    -- * The Writer monad
    Writer,
    runWriter,
    execWriter,
    mapWriter,
    -- * The WriterT monad transformer
    WriterT,
    execWriterT,
    mapWriterT,
    module Control.Monad.Trans,
  ) where

import qualified Control.Monad.Writer.Class as MonadWriter
import Control.Monad.Trans
import Control.Monad.Trans.Writer.CPS (
        Writer, runWriter, execWriter, mapWriter,
        WriterT, execWriterT, mapWriterT)
import Control.Monad.Trans.Writer.CPS qualified as CPS

instance (Monoid w, Monad m) => MonadWriter.MonadWriter w (CPS.WriterT w m) where
  writer = CPS.writer
  tell   = CPS.tell
  listen = CPS.listen
  pass   = CPS.pass
