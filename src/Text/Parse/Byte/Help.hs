module Text.Parse.Byte.Help where

import Text.Parse.Byte (Parser, satisfy, takeWhileP, char, takeWhile1P, tokens, string, (<?>))

import Data.ByteString qualified as T
import Data.Word (Word8)
import Data.Scientific (Scientific)
import Data.Scientific qualified as Sci
import Data.Functor (void)
import Control.Monad.Combinators (choice, option)
import Data.Foldable (Foldable (foldl'))
import Control.Applicative (Alternative ((<|>)))
import Data.CaseInsensitive qualified as CI
import Data.Function (on)

type Tokens = T.ByteString
type Token = Word8

try :: Parser a -> Parser a
try = id
{-# inline try #-}

hidden :: Parser a -> Parser a
hidden = id
{-# inline hidden #-}

-- | Parse and return a single token. It's a good idea to attach a 'label'
-- to this parser.
--
-- > anySingle = satisfy (const True)
--
-- See also: 'satisfy', 'anySingleBut'.
--
-- @since 7.0.0
anySingle :: Parser Token
anySingle = satisfy (const True)
{-# INLINE anySingle #-}

-- | Match any token but the given one. It's a good idea to attach a 'label'
-- to this parser.
--
-- > anySingleBut t = satisfy (/= t)
--
-- See also: 'single', 'anySingle', 'satisfy'.
--
-- @since 7.0.0
anySingleBut ::
  -- | Token we should not match
  Token ->
  Parser Token
anySingleBut t = satisfy (/= t)
{-# INLINE anySingleBut #-}

-- | @'oneOf' ts@ succeeds if the current token is in the supplied
-- collection of tokens @ts@. Returns the parsed token. Note that this
-- parser cannot automatically generate the “expected” component of error
-- message, so usually you should label it manually with 'label' or ('<?>').
--
-- > oneOf cs = satisfy (`elem` cs)
--
-- See also: 'satisfy'.
--
-- > digit = oneOf ['0'..'9'] <?> "digit"
--
-- __Performance note__: prefer 'satisfy' when you can because it's faster
-- when you have only a couple of tokens to compare to:
--
-- > quoteFast = satisfy (\x -> x == '\'' || x == '\"')
-- > quoteSlow = oneOf "'\""
--
-- @since 7.0.0
oneOf ::
  (Foldable f) =>
  -- | Collection of matching tokens
  f Token ->
  Parser Token
oneOf cs = satisfy (`elem` cs)
{-# INLINE oneOf #-}

-- | The same as 'string', but case-insensitive. On success returns string
-- cased as the parsed input.
--
-- >>> parseTest (string' "foobar") "foObAr"
-- "foObAr"
string' :: Tokens -> Parser Tokens
string' = tokens ((==) `on` CI.mk)
{-# INLINE string' #-}

-- | Parse a newline character.
newline :: Parser Token
newline = char 10
{-# INLINE newline #-}

-- | Parse a carriage return character followed by a newline character.
-- Return the sequence of characters parsed.
crlf :: Parser Tokens
crlf = string (T.pack [13, 10])
{-# INLINE crlf #-}

-- | Parse a CRLF (see 'crlf') or LF (see 'newline') end of line. Return the
-- sequence of characters parsed.
eol :: Parser Tokens
eol =
  (T.singleton <$> newline)
    <|> crlf
    <?> "end of line"
{-# INLINE eol #-}

-- | Skip /zero/ or more white space characters.
--
-- See also: 'skipMany' and 'spaceChar'.
space :: Parser ()
space = void $ takeWhileP isSpace
{-# INLINE space #-}

-- | Skip /one/ or more white space characters.
--
-- See also: 'skipSome' and 'spaceChar'.
space1 :: Parser ()
space1 = void $ takeWhile1P (Just "white space") isSpace
{-# INLINE space1 #-}

-- | 'Word8'-specialized version of 'Data.Char.isSpace'.
isSpace :: Token -> Bool
isSpace x
  | x >= 9 && x <= 13 = True
  | x == 32 = True
  | x == 160 = True
  | otherwise = False
{-# INLINE isSpace #-}

-- | The same as 'char' but case-insensitive. This parser returns the
-- actually parsed character preserving its case.
--
-- >>> parseTest (char' 101) "E"
-- 69 -- 'E'
-- >>> parseTest (char' 101) "G"
-- 1:1:
-- unexpected 'G'
-- expecting 'E' or 'e'
char' :: Token -> Parser Token
char' c =
  choice
    [ char (toLower c),
      char (toUpper c)
    ]
{-# INLINE char' #-}

-- | Convert a byte to its upper-case version.
toUpper :: Token -> Token
toUpper x
  | x >= 97 && x <= 122 = x - 32
  | x == 247 = x -- division sign
  | x == 255 = x -- latin small letter y with diaeresis
  | x >= 224 = x - 32
  | otherwise = x
{-# INLINE toUpper #-}

-- | Convert a byte to its lower-case version.
toLower :: Token -> Token
toLower x
  | x >= 65 && x <= 90 = x + 32
  | x == 215 = x -- multiplication sign
  | x >= 192 && x <= 222 = x + 32
  | otherwise = x
{-# INLINE toLower #-}

-- | This is a wrapper for lexemes. The typical usage is to supply the first
-- argument (parser that consumes white space, probably defined via 'space')
-- and use the resulting function to wrap parsers for every lexeme.
--
-- > lexeme  = L.lexeme spaceConsumer
-- > integer = lexeme L.decimal
lexeme ::
  -- | How to consume white space after lexeme
  Parser () ->
  -- | How to parse actual lexeme
  Parser a ->
  Parser a
lexeme spc p = p <* spc
{-# INLINEABLE lexeme #-}

-- | This is a helper to parse symbols, i.e. verbatim strings. You pass the
-- first argument (parser that consumes white space, probably defined via
-- 'space') and then you can use the resulting function to parse strings:
--
-- > symbol    = L.symbol spaceConsumer
-- >
-- > parens    = between (symbol "(") (symbol ")")
-- > braces    = between (symbol "{") (symbol "}")
-- > angles    = between (symbol "<") (symbol ">")
-- > brackets  = between (symbol "[") (symbol "]")
-- > semicolon = symbol ";"
-- > comma     = symbol ","
-- > colon     = symbol ":"
-- > dot       = symbol "."
symbol ::
  -- | How to consume white space after lexeme
  Parser () ->
  -- | Symbol to parse
  Tokens ->
  Parser Tokens
symbol spc = lexeme spc . string
{-# INLINEABLE symbol #-}

-- | Parse a floating point value as a 'Scientific' number. 'Scientific' is
-- great for parsing of arbitrary precision numbers coming from an untrusted
-- source. See documentation in "Data.Scientific" for more information.
--
-- The parser can be used to parse integers or floating point values. Use
-- functions like 'Data.Scientific.floatingOrInteger' from "Data.Scientific"
-- to test and extract integer or real values.
--
-- This function does not parse sign, if you need to parse signed numbers,
-- see 'signed'.
scientific :: Parser Scientific
scientific = do
  c' <- decimal_
  SP c e' <- option (SP c' 0) (try $ dotDecimal_ c')
  e <- option e' (try $ exponent_ e')
  return (Sci.scientific c e)
{-# INLINEABLE scientific #-}

data SP = SP !Integer {-# UNPACK #-} !Int

-- | Parse an integer in the decimal representation according to the format
-- of integer literals described in the Haskell report.
--
-- If you need to parse signed integers, see the 'signed' combinator.
--
-- __Warning__: this function does not perform range checks.
decimal :: (Num a) => Parser a
decimal = decimal_ <?> "integer"
{-# INLINEABLE decimal #-}

-- | A non-public helper to parse decimal integers.
decimal_ :: (Num a) => Parser a
decimal_ = mkNum <$> takeWhile1P (Just "digit") isDigit
  where
    mkNum :: (Num a) => Tokens -> a
    mkNum = foldl' step 0 . T.unpack
    step :: (Num a) => a -> Token -> a
    step a c = a * 10 + fromIntegral (c - 48)
{-# INLINE decimal_ #-}

-- | A fast predicate to check if the given 'Word8' is a digit in ASCII.
isDigit :: Token -> Bool
isDigit w = w - 48 < 10
{-# INLINE isDigit #-}

dotDecimal_ :: Integer -> Parser SP
dotDecimal_ c' = do
  void (char 46)
  let mkNum = foldl' step (SP c' 0) . T.unpack
      step :: SP -> Token -> SP
      step (SP a e') c =
        SP
          (a * 10 + fromIntegral (c - 48))
          (e' - 1)
  mkNum <$> takeWhile1P (Just "digit") isDigit
{-# INLINE dotDecimal_ #-}

exponent_ :: Int -> Parser Int
exponent_ e' = do
  void (char' 101)
  (+ e') <$> signed (return ()) decimal_
{-# INLINE exponent_ #-}

-- | @'signed' space p@ parses an optional sign character (“+” or “-”), then
-- if there is a sign it consumes optional white space (using the @space@
-- parser), then it runs the parser @p@ which should return a number. Sign
-- of the number is changed according to the previously parsed sign
-- character.
--
-- For example, to parse signed integer you can write:
--
-- > lexeme        = L.lexeme spaceConsumer
-- > integer       = lexeme L.decimal
-- > signedInteger = L.signed spaceConsumer integer
signed ::
  (Num a) =>
  -- | How to consume white space after the sign
  Parser () ->
  -- | How to parse the number itself
  Parser a ->
  -- | Parser for signed numbers
  Parser a
signed spc p = option id (lexeme spc sign) <*> p
  where
    sign :: (Num a) => Parser (a -> a)
    sign = (id <$ char 43) <|> (negate <$ char 45)
{-# INLINEABLE signed #-}
