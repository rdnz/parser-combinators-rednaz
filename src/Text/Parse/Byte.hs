module Text.Parse.Byte where

import Data.ByteString qualified as T
import Data.Word (Word8)
import Data.Text (unpack)
import Data.Text.Encoding (decodeUtf8')
import Control.Exception (displayException)
import Control.Applicative (Alternative)
import Control.DeepSeq (NFData)
import Control.Monad (MonadPlus, unless)
import Control.Monad.State.Strict (MonadState (get, put), StateT (StateT), runStateT)
import Control.Monad.Trans.Maybe (MaybeT, runMaybeT)
import Control.Monad.Writer.CPS (MonadWriter (tell), Writer, runWriter)
import Data.Bifunctor (first, second)
import Data.Coerce (coerce)
import Data.DifferenceList (DifferenceList)
import Data.DifferenceList qualified as D
import Data.Foldable (Foldable (toList))
import Data.Functor (($>))
import Data.List qualified as L
import GHC.Generics (Generic)
import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.List.NonEmpty qualified as N
import Data.Maybe (fromMaybe)
import Utilities (catch, (.:))

type Tokens = T.ByteString
type Token = Word8

fastLength :: Tokens -> Int
fastLength = T.length
{-# inline fastLength #-}

newtype Parser a =
  Parser ((StateT Tokens (MaybeT (Writer (DifferenceList ParseError)))) a)
  deriving newtype (Functor, Applicative, Alternative, Monad, MonadState Tokens, MonadWriter (DifferenceList ParseError), MonadPlus)

data ParseError =
  ParseError
    Tokens -- ^ expected
    Tokens -- ^ actual
  |
  Fail
    String -- ^ expected
    Tokens -- ^ actual
  deriving stock (Eq)

data GroupedParseError =
  GroupedParseError
    Tokens -- ^ expected
  |
  GroupedFail
    String -- ^ expected
  deriving stock (Eq, Ord, Show, Generic)

instance NFData GroupedParseError

throw :: DifferenceList ParseError -> Parser a
throw errors = tell errors *> Parser (fail "throw")
{-# inline throw #-}

instance MonadFail (Parser) where
  fail text =
    do
      input <- get
      throw $ D.singleton $ Fail text input

runParser ::
  Parser a ->
  Tokens ->
  Either [(Tokens, NonEmpty GroupedParseError)] a
runParser =
  -- Either [(Tokens, NonEmpty GroupedParseError)] a
  (first . fmap . fmap) (fmap N.head . N.group1 . N.sortBy compare) .:
  -- Either [(Tokens, NonEmpty GroupedParseError)] a
  first (
    -- [(Tokens, NonEmpty GroupedParseError)]
    fmap (\es@(eHead :| _) -> (inputRestGet eHead, toGrouped <$> es)) .
    -- [NonEmpty ParseError]
    N.groupWith (fastLength . inputRestGet)
  ) .:
  -- Either [ParseError] a
  first (L.sortOn (fastLength . inputRestGet)) .:
  -- Either [ParseError] a
  first toList .:
  -- Either (DifferenceList ParseError) a
  second fst .:
  -- Either (DifferenceList ParseError) (a, Tokens)
  (\case
    (Nothing, errors) -> Left errors
    (Just a, _) -> Right a
  ) .:
  -- (DifferenceList ParseError, Maybe (a, Tokens))
  runParserVerbose
{-# inline runParser #-}

runParserVerbose ::
  forall a.
  Parser a ->
  Tokens ->
  (Maybe (a, Tokens), DifferenceList ParseError)
runParserVerbose =
  coerce @((StateT Tokens (MaybeT (Writer (DifferenceList ParseError)))) a -> Tokens -> (Maybe (a, Tokens), DifferenceList ParseError)) $ -- ignore
    runWriter .: runMaybeT .: runStateT
{-# inline runParserVerbose #-}

inputRestGet :: ParseError -> Tokens
inputRestGet (ParseError _ input) = input
inputRestGet (Fail _ input) = input
{-# inline inputRestGet #-}

label :: String -> Parser a -> Parser a
label text =
  handle
    (do
      input <- get
      throw $ D.singleton $ Fail text input
    )
{-# inline label #-}

lookAhead :: Parser a -> Parser a
lookAhead parser =
  do
    input <- get
    parser <* put input
{-# inline lookAhead #-}

notFollowedBy :: Parser a -> Parser ()
notFollowedBy parser =
  (\case
    Nothing -> pure ()
    Just _ -> fail "expected parser to fail"
  ) =<<
  handle (pure Nothing) (Just <$> parser)
{-# inline notFollowedBy #-}    

eof :: Parser ()
eof =
  do
    input <- get
    unless (T.null input) (fail "end of input")
{-# inline eof #-}

token :: Maybe String -> (Token -> Maybe a) -> Parser a
token labelMaybe process =
  do
    input <- get
    case T.uncons input of
      Nothing ->
        failure
      Just (tokenNext, inputRest) ->
        case process tokenNext of
          Just result -> put inputRest $> result
          Nothing -> failure
  where
    failure :: Parser a
    failure =
      fail
        (fromMaybe "a token accepted by predicate argument" labelMaybe)

{-# inline token #-}

single :: Token -> Parser Token
single token0 =
  do
    input <- get
    let
      failure :: Parser a
      failure = throw $ D.singleton $ ParseError (T.singleton token0) input
    case T.uncons input of
      Nothing ->
        failure
      Just (tokenNext, inputRest) ->
        if tokenNext == token0 then put inputRest $> tokenNext else failure
{-# inline single #-}

tokens :: (Tokens -> Tokens -> Bool) -> Tokens -> Parser Tokens
tokens equals expected =
  do
    input <- get
    let (inputNext, inputRest) = T.splitAt (T.length expected) input
    if expected `equals` inputNext
      then put inputRest $> inputNext
      else throw $ D.singleton $ ParseError expected input
{-# inline tokens #-}

takeWhileP :: (Token -> Bool) -> Parser Tokens
takeWhileP predicate =
  do
    input <- get
    let (inputNext, inputRest) = T.span predicate input
    put inputRest $> inputNext
{-# inline takeWhileP #-}

takeWhile1P :: Maybe String -> (Token -> Bool) -> Parser Tokens
takeWhile1P labelMaybe predicate =
  do
    input <- get
    let (inputNext, inputRest) = T.span predicate input
    if T.null inputNext
      then
        fail
          (fromMaybe
            "one or more tokens accepted by predicate argument"
            labelMaybe
          )
      else
        put inputRest $> inputNext
{-# inline takeWhile1P #-}

takeP :: Maybe String -> Int -> Parser Tokens
takeP labelMaybe n =
  do
    input <- get
    if n <= T.length input
      then
        let (inputNext, inputRest) = T.splitAt n input
        in put inputRest $> inputNext
      else
        fail (show n <> " " <> fromMaybe "tokens" labelMaybe)
{-# inline takeP #-}

(<?>) :: Parser a -> String -> Parser a
(<?>) = flip label
{-# inline (<?>) #-}

satisfy :: (Token -> Bool) -> Parser Token
satisfy predicate =
  token
    Nothing
    (\token0 -> if predicate token0 then Just token0 else Nothing)
{-# inline satisfy #-}

char :: Token -> Parser Token
char = single
{-# inline char #-}

string :: Tokens -> Parser Tokens
string = tokens (==)
{-# inline string #-}

handle :: forall a. Parser a -> Parser a -> Parser a
handle =
  coerce @((StateT Tokens (MaybeT (Writer (DifferenceList ParseError)))) a -> (StateT Tokens (MaybeT (Writer (DifferenceList ParseError)))) a -> (StateT Tokens (MaybeT (Writer (DifferenceList ParseError)))) a) $ -- ignore
    flip catch
{-# inline handle #-}

-- to do. calculate line and column numbers
errorBundlePretty :: [(Tokens, NonEmpty GroupedParseError)] -> String
errorBundlePretty =
  foldMap
    (\(actual, errors) ->
      "unexpected " <>
      (
        unpack $
        fromRight (error . displayException) $
        decodeUtf8' $
        T.take 100 $
        actual
      ) <>
      "\n" <>
      "expecting " <> show (toList errors) <> "\n\n"
    )

toGrouped :: ParseError -> GroupedParseError
toGrouped (ParseError expected _actual) = GroupedParseError expected
toGrouped (Fail expected _actual) = GroupedFail expected
{-# inline toGrouped #-}

fromRight :: (a -> b) -> Either a b -> b
fromRight f = either f id
