module Text.Parse.Char.Help where

import Text.Parse.Char (Parser, satisfy, takeWhileP, char, takeWhile1P, tokens, lookAhead, string, label, (<?>))

import Data.Text qualified as T
import Data.Char (isSpace, toLower, toUpper, toTitle, isDigit, digitToInt, readLitChar, isAlphaNum, isLower, isUpper)
import Data.Maybe (listToMaybe)
import Control.Monad.Combinators (count', skipCount, skipMany, manyTill)
import Data.Scientific (Scientific)
import Data.Scientific qualified as Sci
import Data.Functor (void)
import Control.Monad.Combinators (choice, option)
import Data.Foldable (Foldable (foldl'))
import Control.Applicative (Alternative ((<|>)))
import Data.CaseInsensitive qualified as CI
import Data.Function (on)

type Tokens = T.Text
type Token = Char

try :: Parser a -> Parser a
try = id
{-# inline try #-}

hidden :: Parser a -> Parser a
hidden = id
{-# inline hidden #-}

-- | Parse and return a single token. It's a good idea to attach a 'label'
-- to this parser.
--
-- > anySingle = satisfy (const True)
--
-- See also: 'satisfy', 'anySingleBut'.
--
-- @since 7.0.0
anySingle :: Parser Token
anySingle = satisfy (const True)
{-# INLINE anySingle #-}

-- | Match any token but the given one. It's a good idea to attach a 'label'
-- to this parser.
--
-- > anySingleBut t = satisfy (/= t)
--
-- See also: 'single', 'anySingle', 'satisfy'.
--
-- @since 7.0.0
anySingleBut ::
  -- | Token we should not match
  Token ->
  Parser Token
anySingleBut t = satisfy (/= t)
{-# INLINE anySingleBut #-}

-- | @'oneOf' ts@ succeeds if the current token is in the supplied
-- collection of tokens @ts@. Returns the parsed token. Note that this
-- parser cannot automatically generate the “expected” component of error
-- message, so usually you should label it manually with 'label' or ('<?>').
--
-- > oneOf cs = satisfy (`elem` cs)
--
-- See also: 'satisfy'.
--
-- > digit = oneOf ['0'..'9'] <?> "digit"
--
-- __Performance note__: prefer 'satisfy' when you can because it's faster
-- when you have only a couple of tokens to compare to:
--
-- > quoteFast = satisfy (\x -> x == '\'' || x == '\"')
-- > quoteSlow = oneOf "'\""
--
-- @since 7.0.0
oneOf ::
  (Foldable f) =>
  -- | Collection of matching tokens
  f Token ->
  Parser Token
oneOf cs = satisfy (`elem` cs)
{-# INLINE oneOf #-}

-- | The same as 'string', but case-insensitive. On success returns string
-- cased as the parsed input.
--
-- >>> parseTest (string' "foobar") "foObAr"
-- "foObAr"
string' :: Tokens -> Parser Tokens
string' = tokens ((==) `on` CI.mk)
{-# INLINE string' #-}

-- | Parse a newline character.
newline :: Parser Token
newline = char '\n'
{-# INLINE newline #-}

-- | Parse a carriage return character followed by a newline character.
-- Return the sequence of characters parsed.
crlf :: Parser Tokens
crlf = string (T.pack "\r\n")
{-# INLINE crlf #-}

-- | Parse a CRLF (see 'crlf') or LF (see 'newline') end of line. Return the
-- sequence of characters parsed.
eol :: Parser Tokens
eol =
  (T.singleton <$> newline)
    <|> crlf
    <?> "end of line"
{-# INLINE eol #-}

-- | Skip /zero/ or more white space characters.
--
-- See also: 'skipMany' and 'spaceChar'.
space :: Parser ()
space = void $ takeWhileP isSpace
{-# INLINE space #-}

-- | Skip /one/ or more white space characters.
--
-- See also: 'skipSome' and 'spaceChar'.
space1 :: Parser ()
space1 = void $ takeWhile1P (Just "white space") isSpace
{-# INLINE space1 #-}

-- | Parse an upper-case or title-case alphabetic Unicode character. Title
-- case is used by a small number of letter ligatures like the
-- single-character form of Lj.
upperChar :: Parser Token
upperChar = satisfy isUpper <?> "uppercase letter"
{-# INLINE upperChar #-}

-- | Parse a lower-case alphabetic Unicode character.
lowerChar :: Parser Token
lowerChar = satisfy isLower <?> "lowercase letter"
{-# INLINE lowerChar #-}

-- | Parse an alphabetic or numeric digit Unicode characters.
--
-- Note that the numeric digits outside the ASCII range are parsed by this
-- parser but not by 'digitChar'. Such digits may be part of identifiers but
-- are not used by the printer and reader to represent numbers.
alphaNumChar :: Parser Token
alphaNumChar = satisfy isAlphaNum <?> "alphanumeric character"
{-# INLINE alphaNumChar #-}

-- | The same as 'char' but case-insensitive. This parser returns the
-- actually parsed character preserving its case.
--
-- >>> parseTest (char' 'e') "E"
-- 'E'
-- >>> parseTest (char' 'e') "G"
-- 1:1:
-- unexpected 'G'
-- expecting 'E' or 'e'
char' :: Token -> Parser Token
char' c =
  choice
    [ char (toLower c),
      char (toUpper c),
      char (toTitle c)
    ]
{-# INLINE char' #-}

-- | The lexeme parser parses a single literal character without quotes. The
-- purpose of this parser is to help with parsing of conventional escape
-- sequences. It's your responsibility to take care of character literal
-- syntax in your language (by surrounding it with single quotes or
-- similar).
--
-- The literal character is parsed according to the grammar rules defined in
-- the Haskell report.
--
-- Note that you can use this parser as a building block to parse various
-- string literals:
--
-- > stringLiteral = char '"' >> manyTill L.charLiteral (char '"')
--
-- __Performance note__: the parser is not particularly efficient at the
-- moment.
charLiteral :: Parser Token
charLiteral = label "literal character" $ do
  -- The @~@ is needed to avoid requiring a MonadFail constraint,
  -- and we do know that r will be non-empty if count' succeeds.
  r <- lookAhead (count' 1 10 anySingle)
  case listToMaybe (readLitChar r) of
    Just (c, r') -> c <$ skipCount (length r - length r') anySingle
    Nothing -> fail "expected literal character"
{-# INLINEABLE charLiteral #-}

-- | This is a wrapper for lexemes. The typical usage is to supply the first
-- argument (parser that consumes white space, probably defined via 'space')
-- and use the resulting function to wrap parsers for every lexeme.
--
-- > lexeme  = L.lexeme spaceConsumer
-- > integer = lexeme L.decimal
lexeme ::
  -- | How to consume white space after lexeme
  Parser () ->
  -- | How to parse actual lexeme
  Parser a ->
  Parser a
lexeme spc p = p <* spc
{-# INLINEABLE lexeme #-}

-- | This is a helper to parse symbols, i.e. verbatim strings. You pass the
-- first argument (parser that consumes white space, probably defined via
-- 'space') and then you can use the resulting function to parse strings:
--
-- > symbol    = L.symbol spaceConsumer
-- >
-- > parens    = between (symbol "(") (symbol ")")
-- > braces    = between (symbol "{") (symbol "}")
-- > angles    = between (symbol "<") (symbol ">")
-- > brackets  = between (symbol "[") (symbol "]")
-- > semicolon = symbol ";"
-- > comma     = symbol ","
-- > colon     = symbol ":"
-- > dot       = symbol "."
symbol ::
  -- | How to consume white space after lexeme
  Parser () ->
  -- | Symbol to parse
  Tokens ->
  Parser Tokens
symbol spc = lexeme spc . string
{-# INLINEABLE symbol #-}

-- | @'spaceAndComments' sc lineComment blockComment@ produces a parser that can parse
-- white space in general. It's expected that you create such a parser once
-- and pass it to other functions in this module as needed (when you see
-- @spaceConsumer@ in documentation, usually it means that something like
-- 'space' is expected there).
--
-- @sc@ is used to parse blocks of space characters. You can use
-- 'Text.Megaparsec.Char.space1' from "Text.Megaparsec.Char" for this
-- purpose as well as your own parser (if you don't want to automatically
-- consume newlines, for example). Make sure that the parser does not
-- succeed on the empty input though. In an earlier version of the library
-- 'Text.Megaparsec.Char.spaceChar' was recommended, but now parsers based
-- on 'takeWhile1P' are preferred because of their speed.
--
-- @lineComment@ is used to parse line comments. You can use
-- @skipLineComment@ if you don't need anything special.
--
-- @blockComment@ is used to parse block (multi-line) comments. You can use
-- @skipBlockComment@ or @skipBlockCommentNested@ if you don't need anything
-- special.
--
-- If you don't want to allow a kind of comment, simply pass 'empty' which
-- will fail instantly when parsing of that sort of comment is attempted and
-- 'spaceAndComments' will just move on or finish depending on whether there is more
-- white space for it to consume.
spaceAndComments ::
  -- | A parser for space characters which does not accept empty
  -- input (e.g. 'Text.Megaparsec.Char.space1')
  Parser () ->
  -- | A parser for a line comment (e.g. 'skipLineComment')
  Parser () ->
  -- | A parser for a block comment (e.g. 'skipBlockComment')
  Parser () ->
  Parser ()
spaceAndComments sp line block =
  skipMany $
    choice
      [hidden sp, hidden line, hidden block]
{-# INLINEABLE spaceAndComments #-}

-- | Given a comment prefix this function returns a parser that skips line
-- comments. Note that it stops just before the newline character but
-- doesn't consume the newline. Newline is either supposed to be consumed by
-- 'space' parser or picked up manually.
skipLineComment ::
  -- | Line comment prefix
  Tokens ->
  Parser ()
skipLineComment prefix =
  string prefix *> void (takeWhileP (/= '\n'))
{-# INLINEABLE skipLineComment #-}

-- | @'skipBlockComment' start end@ skips non-nested block comment starting
-- with @start@ and ending with @end@.
skipBlockComment ::
  -- | Start of block comment
  Tokens ->
  -- | End of block comment
  Tokens ->
  Parser ()
skipBlockComment start end = p >> void (manyTill anySingle n)
  where
    p = string start
    n = string end
{-# INLINEABLE skipBlockComment #-}

-- | @'skipBlockCommentNested' start end@ skips possibly nested block
-- comment starting with @start@ and ending with @end@.
--
-- @since 5.0.0
skipBlockCommentNested ::
  -- | Start of block comment
  Tokens ->
  -- | End of block comment
  Tokens ->
  Parser ()
skipBlockCommentNested start end = p >> void (manyTill e n)
  where
    e = skipBlockCommentNested start end <|> void anySingle
    p = string start
    n = string end
{-# INLINEABLE skipBlockCommentNested #-}

-- | Parse a floating point value as a 'Scientific' number. 'Scientific' is
-- great for parsing of arbitrary precision numbers coming from an untrusted
-- source. See documentation in "Data.Scientific" for more information.
--
-- The parser can be used to parse integers or floating point values. Use
-- functions like 'Data.Scientific.floatingOrInteger' from "Data.Scientific"
-- to test and extract integer or real values.
--
-- This function does not parse sign, if you need to parse signed numbers,
-- see 'signed'.
scientific :: Parser Scientific
scientific = do
  c' <- decimal_
  SP c e' <- option (SP c' 0) (try $ dotDecimal_ c')
  e <- option e' (try $ exponent_ e')
  return (Sci.scientific c e)
{-# INLINEABLE scientific #-}

data SP = SP !Integer {-# UNPACK #-} !Int

-- | Parse an integer in the decimal representation according to the format
-- of integer literals described in the Haskell report.
--
-- If you need to parse signed integers, see the 'signed' combinator.
--
-- __Warning__: this function does not perform range checks.
decimal :: (Num a) => Parser a
decimal = decimal_ <?> "integer"
{-# INLINEABLE decimal #-}

-- | A non-public helper to parse decimal integers.
decimal_ :: (Num a) => Parser a
decimal_ = mkNum <$> takeWhile1P (Just "digit") isDigit
  where
    mkNum :: (Num a) => Tokens -> a
    mkNum = foldl' step 0 . T.unpack
    step :: (Num a) => a -> Token -> a
    step a c = a * 10 + fromIntegral (digitToInt c)
{-# INLINE decimal_ #-}

dotDecimal_ :: Integer -> Parser SP
dotDecimal_ c' = do
  void (char '.')
  let mkNum = foldl' step (SP c' 0) . T.unpack
      step :: SP -> Token -> SP
      step (SP a e') c =
        SP
          (a * 10 + fromIntegral (digitToInt c))
          (e' - 1)
  mkNum <$> takeWhile1P (Just "digit") isDigit
{-# INLINE dotDecimal_ #-}

exponent_ :: Int -> Parser Int
exponent_ e' = do
  void (char' 'e')
  (+ e') <$> signed (return ()) decimal_
{-# INLINE exponent_ #-}

-- | @'signed' space p@ parses an optional sign character (“+” or “-”), then
-- if there is a sign it consumes optional white space (using the @space@
-- parser), then it runs the parser @p@ which should return a number. Sign
-- of the number is changed according to the previously parsed sign
-- character.
--
-- For example, to parse signed integer you can write:
--
-- > lexeme        = L.lexeme spaceConsumer
-- > integer       = lexeme L.decimal
-- > signedInteger = L.signed spaceConsumer integer
signed ::
  (Num a) =>
  -- | How to consume white space after the sign
  Parser () ->
  -- | How to parse the number itself
  Parser a ->
  -- | Parser for signed numbers
  Parser a
signed spc p = option id (lexeme spc sign) <*> p
  where
    sign :: (Num a) => Parser (a -> a)
    sign = (id <$ char '+') <|> (negate <$ char '-')
{-# INLINEABLE signed #-}
