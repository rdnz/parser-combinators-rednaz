{-# options_ghc -Wno-missing-import-lists #-}

module Text.Parse.Char.AttoparsecInterface
  (
    module Text.Parse.Char,
    module Text.Parse.Char.AttoparsecInterface,
  )
  where

import Text.Parse.Char
import Text.Parse.Char.Help (space, anySingle)

import Data.Text qualified as T
import Control.Applicative (Alternative ((<|>)))
import Data.Char (isDigit, isLetter, ord)

skipSpace :: Parser ()
skipSpace = space
{-# inline skipSpace #-}

anyChar :: Parser Token
anyChar = anySingle
{-# inline anyChar #-}

endOfLine :: Parser ()
endOfLine = char '\n' *> pure () <|> string "\r\n" *> pure ()
{-# inline endOfLine #-}

endOfInput :: Parser ()
endOfInput = eof

decimal :: Integral a => Parser a
decimal =
  T.foldl' step 0 <$> takeWhile1P (Just "digit") isDigit <?> "decimal"
  where
    step :: Integral a => a -> Char -> a
    step a c = a * 10 + fromIntegral (ord c - 48)
{-# SPECIALISE decimal :: Parser Int #-}
{-# SPECIALISE decimal :: Parser Word #-}
{-# SPECIALISE decimal :: Parser Integer #-}
{-# INLINE decimal #-}

-- | Parse a number with an optional leading @\'+\'@ or @\'-\'@ sign
-- character.
signed :: Num a => Parser a -> Parser a
{-# SPECIALISE signed :: Parser Int -> Parser Int #-}
{-# SPECIALISE signed :: Parser Integer -> Parser Integer #-}
signed p = (negate <$> (char '-' *> p))
       <|> (char '+' *> p)
       <|> p

digit :: Parser Char
digit = satisfy isDigit <?> "digit"
{-# INLINE digit #-}

letter :: Parser Char
letter = satisfy isLetter <?> "letter"
{-# INLINE letter #-}
