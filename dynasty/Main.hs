module Main where

import System.Directory(listDirectory)

import Dynasty.Megaparsec qualified as M
import Dynasty.Rednaz qualified as R
import System.Exit(exitFailure)
import Data.Text(Text)
import Data.Text.IO qualified as T
import Control.Monad(when)

getModules :: IO [(FilePath, Text)]
getModules = do
  files <- listDirectory "examples"
  traverse (\f -> (f,) <$> T.readFile ("examples/" <> f)) files

main :: IO ()
main = do
  modules <- getModules

  let
    a = M.parse modules
    b = R.parse modules

  when (a /= b) do
    print a
    print b
    exitFailure
